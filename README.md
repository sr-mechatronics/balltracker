#JHU Mechatronics (530.421) - Spring 2016

@sreichenstein

Baxter Eldridge



Laboratory 4 - Ball Tracker
===========================

#Objective
The objective of this lab is to provide an introduction to vision-based sensory information using the Pixy camera. The camera and Arduino were used to sense the number and color of balls in an arena.

#Design Requirements
The automatic ball tracking system is required to use the Pixy camera to recognize and continuously track colored balls dropped from above into in an artificial arena. The system tracks 3 colors, blue, green, and other, and keeps count of how many of each color are seen. The number of balls must be counted as they fall and then again once they have become stationary. This is because some of the balls dropped will likely bounce out of the arena and field of view (FOV) of the camera, subsequently the number of balls dropped may be less than the number of balls which come to rest in the arena. The system recognizes when balls are stationary and then computes the direct distance to each one from the center of the camera sensor. However, there are also several constraints on the system. The arena is a  2ft x 2ft square, and the system must be placed outside of the arena. Subsequently, the camera cannot be placed above the arena. The Pixy camera must also be taught the color of the "other" balls prior to the balls being dropped into the arena. Finally, the error of the distances determined by the system should not to exceed +/‐ 5% relative to the best results achieved by all teams or 10% of the actual distances, whichever is smaller. 

#Design

The design places the camera at a known height and an angle which is included to put the center of the FOV at approximately the center of the arena. The angle does not need to be measured. The entire assembly is placed outside of the footprint of the arena. Our initial plan was to determine the distance based on the apparent size of the balls based on how far away they are in the X or Y direction. The apparent size of a viewed object depends on the distance from the sensor. That is the apparent size will remain the same if it lies on the surface of a concave dome that is centered about the camera. However, once we actually mounted our camera we found that the size of the box which the camera drew around a ball at a given position was not necessarily consistent from ball to ball or even from frame to frame. We subsequently had to find a new way to determine the ball’s distance from the camera. 

We created a uniform grid in our arena and found the distance from each point to the camera. A ball was placed on each point and recorded the X and Y position given by the Pixy. We noticed that along each row, the Y values did not change significantly whereas the X values did, so we fit a quadratic function to the X values of each set of Y ranges. See plots at end of this document. Unfortunately this calibration needs to be done and the data needs to be re-recorded any time the Pixy is moved at all.

Camera Mount:
![](raw/mount_side.JPG "Camera Mount")
System:
![](raw/system.JPG "System")

#Program
The structure of the program is to first wait until the balls drop and then to count the number of balls. Then, once all the balls have stopped moving, it counts the number of balls still in the frame, then computes the distance from the camera to all those balls.

This first part is accomplished by looping until the number of blocks, or number of recognized objects, returned by the Pixy object is greater than 0. When this happens, we sample a number of frames (125 in our program) and extract the maximum number of recognized signatures in each valid frame (number of blocks > 0). This maximum number of recognized signatures give the number of balls originally dropped into the arena.

Due to how the pixy is designed, it will occasionally return invalid and unexpected data (only recognize a few of the objects). We overcome this by sampling a number of frames (300 in our program) and extracting the mean of the number of blocks out of the valid frames. This works on the assumption that there is more data with the correct number of blocks than otherwise. This mean number of blocks is compared against a previous (but very recently acquired) mean number of blocks. If the number is the same, then the scene has not changed. We poll the Pixy for that number of blocks because it will still output invalid data occasionally, and then load the blocks and compute the distance to each recognized object.

The distance is computed in a very straightforward manner. A distance equation was fit as a function of X for several ranges on the Y axis of the Pixy’s FoV.  A recognized object’s Y position is used to determine which equation to use, and its X is the input to the function. 

The distance mapping as a function of X and Y position explained in our design was used to compute the distance of each recognized object. It uses several ranges on the Y axis of the Pixy’s FoV and then maps the X position using an equation from our calibration.

#Performance
The program is overall very efficient and robust. Determining the number of balls dropped has been perfectly accurate within reason; a mass of balls will obviously obstruct each other and will scatter when they land on each other. Because the Pixy operates at 50Hz, and we compute 125 valid frames, this will take at least 2.5 seconds. The upper time limit depends on how many frames are determined to be invalid (number of blocks equal to 0) and while it does not have a time or frame limit, in real scenarios this step does not take more than 4 seconds.

Getting the correct number of blocks takes 300 valid frames, or at least 6 seconds. Again this does not have an upper bound, however in our experiments it never exceeded 8 seconds. Next, polling the Pixy until it returns the correct number of blocks has a timeout of 10 seconds, but unless the scene has changed will return within 1 or 2 seconds. The distances are then computed and then printed sequentially. 

Overall, the program runs in linear time with the exception of the polling, which means it is very fast and accurate. Unfortunately, because there is no easy way to automatically calibrate the camera, so it must be done every time it moves. Due to the time delays, included to ensure accuracy and consistency, our automatic ball tracking program takes a fair amount of time to run. This time could likely be reduced if more time were available to streamline the process. However, the program does successfully complete the requirements placed on it for this lab.
