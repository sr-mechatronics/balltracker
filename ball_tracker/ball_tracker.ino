/**
 * Stefan Reichenstein
 * Baxter Eldridge
 * 
 * Lab 4 - Ball tracking with Pixy
 */

#include <SPI.h>
#include <Pixy.h>


///////////////////////////////////////////////////////////////////////////////

typedef struct {
  int s;            // Signature (ie color code)
  int x;            // x pos
  int y;            // y pos
  double dist = -1;  // dist from the center of the camera (hypotenus)
} Ball;

///////////////////////////////////////////////////////////////////////////////

// CMUcam5 - Pixy
Pixy cam;               // Init Pixy

///////////////////////////////////////////////////////////////////////////////

// Ball Stats
int num_c0, num_c1, num_c2;         // current number of balls

const int NUM_COLORS = 3;
const int num_objs = 10;  // number of balls of a specific type
Ball c0[num_objs];
Ball c1[num_objs];
Ball c2[num_objs];

// init starting indices of all Ball arrays to 0;
int c0_i = 0, c1_i = 0, c2_i = 0;

char c0_str[] = "blue";
char c1_str[] = "green";
char c2_str[] = "other";

int c_max[NUM_COLORS] = {0, 0, 0};

int prev_num_blocks;

///////////////////////////////////////////////////////////////////////////////


// System vars/flags

const boolean DEBUG = false;
const boolean REFIT_DATA = true;

const long TIMEOUT = 10000; // millis = 100s
const int N_STATS = 300;
const int N_STATS_DROP = 50 * 2.5;

long start;


char str[80];
char float_buffer[8];


///////////////////////////////////////////////////////////////////////////////


void setup() {
  cam.init();
  Serial.begin(9600);
  while (cam.getBlocks() < 1) {
    Serial.println("Waiting for Drop");
  }
  Serial.println("Balls dropped. Finding Max");
  loadInit();
  
  prev_num_blocks = getNumBlocks();
}

void loadInit() {
  int n, s, c_cur[NUM_COLORS] = {0, 0, 0};
  for (int i = 0; i < N_STATS_DROP; ++i) {
    n = cam.getBlocks();
    if (n < 1) { --i; }
    else {
      for (int j = 0; j < n; ++j) {
        switch (cam.blocks[j].signature) {
          case 1:
            c_cur[0] = c_cur[0] + 1;
            break;
          case 2:
            c_cur[1] = c_cur[1] + 1;
            break;
          default:
            c_cur[2] = c_cur[2] + 1;
            break;
        }
      }
      for (int j = 0; j < NUM_COLORS; ++j) {
        if (c_cur[j] > c_max[j]) {
          c_max[j] = c_cur[j];
        }
        c_cur[j] = 0;
      }
    }
    //delay(10);
  }
  #if DEBUG
  Serial.println(c_max[0]);
  Serial.println(c_max[1]);
  Serial.println(c_max[2]);
  #endif
}

void loop() {
  /**/
  // take the mean over 300 frames with >0 blocks
  // loop until that number of blocks is found
  // do calcs
  // repeat
  int num_blocks = getNumBlocks();

  if (prev_num_blocks != num_blocks) {
    prev_num_blocks = num_blocks;
    return;
  }

  int n;
  start = millis();
  n = cam.getBlocks();
  while (n != num_blocks) {
    if (abs(millis() - start) > TIMEOUT) {
      Serial.println("TIMEOUT!!!");
      return;
    }
    n = cam.getBlocks();
  }

  loadBlocks(n);

  printBlockStats();
  Serial.println("\n");
  
  delay(10);
}

int getNumBlocks() {
  int n, int_array[N_STATS] = {};

  // online-ish bucketsort
  for (int i = 0; i < N_STATS; ++i) {
    n = cam.getBlocks();
    if (n < 1) {
      --i;
    } else {
      ++int_array[n];
    }
  }
  int mean_index = 0;
  int mean = int_array[0];
  for (int i = 0; i < N_STATS; ++i) {
    if (int_array[i] > mean) {
      mean = int_array[i];
      mean_index = i;
    }
  }
  return mean_index;
}

double calcDist(Ball b) {
  static const double d0 = 153.4;
  static const double d1 = 132.2;
  static const double d2 = 116.8;
  static const double d3 = 103.6;
  static const double d4 = 92.8;

  static const double m01 = (abs(d0 - d1) / 2);
  static const double m12 = (abs(d1 - d2) / 2);
  static const double m23 = (abs(d2 - d3) / 2);
  static const double m34 = (abs(d3 - d4) / 2);

  boolean range0 = b.y >= (d0 - m01);
  boolean range1 = ((d1 - m12) <= b.y) && (b.y <= (d1 + m01));
  boolean range2 = ((d2 - m23) <= b.y) && (b.y <= (d2 + m12));
  boolean range3 = ((d3 - m34) <= b.y) && (b.y <= (d3 + m23));
  boolean range4 = b.y <= (d4 + m34);
  
  // fucntion of size and position from center
  if (range0) {
    b.dist = 0.0005 * pow(b.x,2) - 0.1459*b.x + 65.424;
  } else if (range1) {
    b.dist = 0.0005 * pow(b.x,2) - 0.155*b.x + 74.422;
  } else if (range2) {
    b.dist = 0.0007 * pow(b.x,2) - 0.1921*b.x + 85.551;
  } else if (range3) {
    b.dist = 0.0007 * pow(b.x,2) - 0.2006*b.x + 95.371;
  } else {// (range4) {
    b.dist = 0.0008 * pow(b.x,2) - 0.2442*b.x + 105.57;
  }
  return b.dist;
}

// prints the distance from the camera to the objects in the scene.
// MUST CALULATE FIRST!!!
void printBlockStats() {
  sprintf(str, "Max %s balls:\t%d", c0_str, c_max[0]);
  Serial.println(str);
  for (int i = 0; i < c0_i; ++i) {
    dtostrf(calcDist(c0[i]), 4, 2, float_buffer);
    sprintf(str, "%s[%i] dist:\t%s", c0_str, i, float_buffer);
    Serial.println(str);
  }
  Serial.println();

  sprintf(str, "Max %s balls:\t%d", c1_str, c_max[1]);
  Serial.println(str);
  for (int i = 0; i < c1_i; ++i) {
    dtostrf(calcDist(c1[i]), 4, 2, float_buffer);
    sprintf(str, "%s[%i] dist:\t%s", c1_str, i, float_buffer);
    Serial.println(str);
  }
  Serial.println();

  sprintf(str, "Max %s balls:\t%d", c2_str, c_max[2]);
  Serial.println(str);
  for (int i = 0; i < c2_i; ++i) {
    dtostrf(calcDist(c2[i]), 4, 2, float_buffer);
    sprintf(str, "%s[%i] dist:\t%s", c2_str, i, float_buffer);
    Serial.println(str);
  }
}

// loads the current scene. puts the previous blcosk into prev
void loadBlocks(int num_blocks) {
  c0_i = c1_i = c2_i = 0;

  int s, w, h;
  for (int i = 0; i < num_blocks; ++i) {
    Ball new_ball;
    new_ball.s = cam.blocks[i].signature;
    new_ball.x = cam.blocks[i].x;
    new_ball.y = cam.blocks[i].y;

    if (new_ball.s == 1) {
      c0[c0_i++] = new_ball;
    } else if (new_ball.s == 2) {
      c1[c1_i++] = new_ball;
    } else {
      c2[c2_i++] = new_ball;
    }
    #if DEBUG
    cam.blocks[i].print();
    #endif
  }
  
}
 
